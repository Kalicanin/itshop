<link rel="stylesheet" href="style.css">
    <?php 
        include_once 'header.php';
        $msg = isset($msg)?$msg:'';
    ?>

    <main>
        <section id="login">
            <form class="login-form" action="../functionality/controller.php" method="post">
                <label for="fname">First name:</label><br>
                <input type="text" id="fname" name="fname" class="input-area"><br>
                <label for="lname">Last name:</label><br>
                <input type="text" id="lname" name="lname" class="input-area"> <br>
                <label for="email">Email:</label><br>
                <input type="text" id="email" name="email" class="input-area"> <br>
                <label for="username">Username:</label><br>
                <input type="text" id="username" name="username" class="input-area"> <br>
                <label for="password">Password::</label><br>
                <input type="password" id="password" name="password" class="input-area"> <br>
                <label for="confirm">Confirm password:</label><br>
                <input type="password" id="confirm" name="confirm" class="input-area"> <br> <br>
                <input type="submit" value="Login" name="action">
            </form>

            <p><?=$msg?></p>
        </section>
    </main>

    <?php
    include_once 'footer.php';
    ?>