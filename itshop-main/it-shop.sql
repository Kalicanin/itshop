-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 24, 2022 at 02:21 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `it-shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id_brand` int(11) NOT NULL,
  `name_brand` varchar(255) NOT NULL,
  `id_manufacturer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id_brand`, `name_brand`, `id_manufacturer`) VALUES
(7, 'Iphone', 4),
(8, 'Dell', 6),
(9, 'LG', 5),
(10, 'Samsung', 3),
(11, 'Huawei', 1),
(12, 'Lenovo', 2),
(13, 'BEAT', 7),
(14, 'Acer', 8),
(15, 'Razer', 9),
(16, 'HP', 10),
(17, 'Motorola', 11),
(18, 'Honor', 12);

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

CREATE TABLE `manufacturers` (
  `id_manufacturer` int(11) NOT NULL,
  `name_manufacturer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manufacturers`
--

INSERT INTO `manufacturers` (`id_manufacturer`, `name_manufacturer`) VALUES
(1, 'Huawei'),
(2, 'Lenovo'),
(3, 'Samsung'),
(4, 'Apple'),
(5, 'LG'),
(6, 'Dell'),
(7, 'BEAT'),
(8, 'Acer'),
(9, 'Razer'),
(10, 'HP'),
(11, 'Motorola'),
(12, 'Honor');

-- --------------------------------------------------------

--
-- Table structure for table `orderdetails`
--

CREATE TABLE `orderdetails` (
  `od_id` int(11) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `order_time` datetime NOT NULL,
  `order_price` int(11) NOT NULL,
  `order_status` int(11) NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orderdetails`
--

INSERT INTO `orderdetails` (`od_id`, `adress`, `country`, `order_time`, `order_price`, `order_status`, `order_id`) VALUES
(1, 'Cacak,Karadjordjeva 10', 'Serbia', '2022-06-24 13:56:00', 200, 2, 2),
(2, 'Trstenik,Cajkina 33', 'Serbia', '2022-06-11 13:58:24', 1362, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `order_time` datetime NOT NULL DEFAULT current_timestamp(),
  `order_price` varchar(255) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `address`, `country`, `order_time`, `order_price`, `id_product`, `id_user`) VALUES
(2, 'Cacak,Karadjordjeva 10', 'Serbia', '2022-06-11 13:51:50', '1399.50', 1, 2),
(3, 'Cacak,Nemanjina 5', 'Serbia', '2022-06-09 15:52:39', '1099.99', 13, 2),
(4, 'Cacak,Zeleznicka 22', 'Serbia', '2022-06-09 15:53:37', '299.99', 8, 1),
(5, 'Trstenik,Cajkina 33', 'Serbia', '2022-06-09 15:53:51', '899.99', 9, 1),
(6, 'Trstenik,Carice Milice 15', 'Serbia', '2022-06-09 15:54:18', '199.99', 2, 2),
(7, 'Kraljevo,Cara Dusana 12', 'Serbia', '2022-06-09 15:54:22', '399.99', 10, 3),
(8, 'Milinka Kušića 11', 'Serbia', '2022-06-09 15:55:05', '560', 4, 3),
(9, 'Bulevar oslobodilaca Čačka', 'Serbia', '2022-06-09 15:55:29', '560', 4, 3),
(10, 'Bulevar oslobodilaca Čačka', 'Serbia', '2022-06-09 15:56:50', '560', 1, 3),
(11, 'Savska promenada 98', 'Serbia', '2022-06-09 15:57:13', '560', 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `id_type` int(11) NOT NULL,
  `id_brand` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `image`, `id_type`, `id_brand`) VALUES
(1, 'Iphone 13 ', '1361.50', 'iphone.png', 3, 7),
(2, 'Razer x3', '199.99', 'razer.png', 4, 15),
(3, 'Xiaomi', '210', 'huaweip50.jpeg', 3, 11),
(4, 'LCD 43\" Smart TV', '560', 'tv.png', 1, 9),
(5, 'Vostro 3500, 15,6\" FHD', '870', 'laptop1.jpg', 2, 8),
(6, 'IdeaPad 14\" Ryzen 3700', '810', 'laptop2.jpg', 2, 12),
(7, 'LED 32\" Full HD', '340', 'tv1.png', 1, 10),
(8, 'HEAT PRO X3', '299.99', 'headphone.jpeg', 4, 13),
(9, 'Acer nitro 5', '899.99', 'acernitro5.png', 2, 14),
(10, 'Motorola edge 30 PRO', '399.99', 'motorola.png', 3, 17),
(11, 'Honor 50 pro', '599.99', 'honor50pro.png', 3, 18),
(12, 'MACBOOK AIR', '2099.99', 'MACBOOKAIR.png', 2, 7),
(13, 'Samsung galaxy s22', '1099.99', 'samsung.png', 3, 10);

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE `product_types` (
  `id_type` int(11) NOT NULL,
  `name_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`id_type`, `name_type`) VALUES
(1, 'television'),
(2, 'laptop'),
(3, 'mobile'),
(4, 'headphones');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_user_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `name`, `surname`, `email`, `username`, `password`, `id_user_type`) VALUES
(1, 'Nikola ', 'Kalicanin', 'nikola.kali@gmail.com', 'kali2504', 'likapikazacio', 1),
(2, 'Marko', 'Knezevic', 'knezevic988@gmail.com', 'uberstain', 'marko12345', 1),
(3, 'Petra', 'Petrovic', 'petra12@gmail.com', 'petraaa1234', 'naucnik94', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id_user_type` int(11) NOT NULL,
  `name_user_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id_user_type`, `name_user_type`) VALUES
(1, 'korisnik'),
(2, 'moderator'),
(3, 'administrator');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id_brand`),
  ADD KEY `id_manufacturer` (`id_manufacturer`);

--
-- Indexes for table `manufacturers`
--
ALTER TABLE `manufacturers`
  ADD PRIMARY KEY (`id_manufacturer`);

--
-- Indexes for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD PRIMARY KEY (`od_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_type` (`id_type`,`id_brand`),
  ADD KEY `id_brands` (`id_brand`);

--
-- Indexes for table `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`id_type`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_user_type` (`id_user_type`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id_user_type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id_brand` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `manufacturers`
--
ALTER TABLE `manufacturers`
  MODIFY `id_manufacturer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `orderdetails`
--
ALTER TABLE `orderdetails`
  MODIFY `od_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `product_types`
--
ALTER TABLE `product_types`
  MODIFY `id_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id_user_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `brands`
--
ALTER TABLE `brands`
  ADD CONSTRAINT `brands_ibfk_1` FOREIGN KEY (`id_manufacturer`) REFERENCES `manufacturers` (`id_manufacturer`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD CONSTRAINT `orderdetails_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`id_product`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`id_brand`) REFERENCES `brands` (`id_brand`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`id_type`) REFERENCES `product_types` (`id_type`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_user_type`) REFERENCES `user_types` (`id_user_type`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
