<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="style.css">

    <title>Home page</title>
  </head>
  <body>


    <header>
    <?php 
        include_once 'header.php';
    ?>    
    </header>

    <main>
        <section id="first">
            <div>
                <div>
                <p>MIDWEEK DEALS</p>
                <h3>SMARTPHONES ACCESSORIES</h3>
                <p>Up to 40% off</p>
                <input type="button" value="Shop now">
                </div>
                <img id="huaweip50" src="image/iphone.png" alt="">
            </div>
            <div>
                <div>
                <p>LIMITED STOCK</p>
                <h3>MACBOOK AIR</h3>
                <p>Starfing from $900.99</p>
                <input type="button" value="Shop now">
                </div>
                <img id="huaweip50" src="image/macbook copy.png" class="iphone" alt="">
            </div>
            <div>
                <div>
                <p>DISCOVER</p>
                <h3>BEAT PRO X3</h3>
                <p>$259.99 $900.99</p>
                <input type="button" value="Shop now">
                </div>
                <img id="huaweip50" src="image/slusalice.jpg" class="iphone" alt="">
            </div>
        </section>
        <section id="second">
            <div>
                 <h2>Our featured Offers</h2>
                 <p>View All ></p>
            </div>
            <div>
                <div class="card">
                    <img src="image/samsung.png" alt="" class="card-img">
                    <div>
                        <p>TABLETS</p>
                        <h4>Samsung s22</h4>
                        <img src="image/rating.png" alt="" class="rating-img">
                        <p>$23.90 <span>$19.12</span></p>
                    </div>
                </div>
                <div class="card">
                    <img src="image/acernitro5.png" alt="" class="card-img">
                    <div>
                        <p>MOBILES</p>
                        <h4>Acer nitro 5 16.3"</h4>
                        <img src="image/rating.png" alt="" class="rating-img">
                        <p>$23.90 <span>$28.72</span></p>
                    </div>
                </div>
                <div class="card">
                    <img src="image/macbook copy.png" alt="" class="card-img">
                    <div>
                        <p>LAPTOPS</p>
                        <h4>macbook air</h4>
                        <img src="image/rating.png" alt="" class="rating-img">
                        <p>$29.00 <span>$27.55</span></p>
                    </div>
                </div>
                <div class="card">
                    <img src="image/hppavilion.png" alt="" class="card-img">
                    <div>
                        <p>LAPTOPS</p>
                        <h4>HP Pavilion X360 Convertibile 14-Dy0064TU</h4>
                        <img src="image/rating.png" alt="" class="rating-img">
                        <p><span>$29.00</span></p>
                    </div>
                </div>
            </div>
        </section>
        <section id="third">
            <div>
                <div>
                    <h2>This Week Deals</h2>
                    <p>144days : 11hours : 11min : 52sec</p>
                </div>
                <p>View All Deals ></p>
            </div>
            <div>
                <div>
                    <img src="image/razer.png" alt="" class="card-img">
                    <div>
                        <p>TABLETS</p>
                        <h4>Razer x3</h4>
                        <img src="image/rating.png" alt="" class="rating-img">
                        <p>$23.90 <span>$19.12</span></p>
                    </div>
                </div>
                <div>
                    <img src="image/lenovoidea.jpeg" alt="" class="card-img">
                    <div>
                        <p>MOBILES</p>
                        <h4>Lenovo IdeaPad S540 13.3"</h4>
                        <img src="image/rating.png" alt="" class="rating-img">
                        <p>$23.90 <span>$28.72</span></p>
                    </div>
                </div>
            </div>
        </section>
        <section id="sponsors">
            <img src="image/quantox-logo.png" alt="" class="sponsors-img">
            <img src="image/quantox-logo.png" alt="" class="sponsors-img">
            <img src="image/quantox-logo.png" alt="" class="sponsors-img">
           
        </section>
    </main>

    <footer>
        <?php
        include_once 'footer.php';
        ?>
    </footer>
 </body>
</html>
