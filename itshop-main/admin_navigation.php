<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
       
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="admin_index.php">ADMIN page</a>
            </div>

<ul class="nav navbar-right top-nav">
   <li><a href="home.php">HOME SITE</a></li>
   
       
    
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> admin <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            
            
   
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="shop.php"><i class="fa fa-fw fa-dashboard"></i>Shop page</a>
                    </li>
                    
                    <li>
                        <a href="register.php"><i class="fa fa-fw fa-wrench"></i> Register</a>
                    </li>
                    
                    <li>
                        <a href="contact.php"><i class="fa fa-fw fa-dashboard"></i>Contact page</a>
                    </li>
                    <li>
                        <a href="login.php"><i class="fa fa-fw fa-wrench"></i> Login</a>
                    </li>
                    
                    <li class="active">
                        <a href="orders.php"><i class="fa fa-fw fa-file"></i> Orders</a>
                    </li>
                  
                   
                    
                    
                </ul>
            </div>
            
      
        </nav>