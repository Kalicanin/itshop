<link rel="stylesheet" href="style.css">
<?php 
        include_once 'header.php';
        require_once 'DAO.php';
    ?>
    <?php
        $dao=new DAO();
        $brands=$dao->selectBrands();
        $manufacturers=$dao->selectManufactures();
        $products=$dao->selectProducts();
        $types=$dao->selectProductTypes();
    ?>
    <main>
        <section id="shop-container">
            <section id="filter-section">
                <div>
                    <h3>Home</h3>
                    <a href="" onclick="showAllProduct()">Show all products</a><br>
                </div>

                <div>
                    <h3>Categories</h3>
                    <?php for($i=0;$i<count($types);$i++) {?>
                    <input type="checkbox" onchange="categoriesListChange(<?= $types[$i]['id_type']?>)" id="categories-<?= $types[$i]['id_type']?>">
                    <label> <?= $types[$i]['name_type']?></label> <br>
                    <?php } ?>
                </div>


                <div>
                    <h3>Brand</h3>
                    <?php for($i=0;$i<count($brands);$i++) {
                        if($brands[$i]['id_brand'])?>
                    <input type="checkbox" onchange="brandListChange(<?= $brands[$i]['id_brand']?>)" id="brand-<?= $brands[$i]['id_brand']?>" value="<?= $brands[$i]['name_brand']?>">
                    <label for="brand-<?= $brands[$i]['id_brand']?>"> <?= $brands[$i]['name_brand']?></label> <br>
                    <?php }?>
                </div>
                <div>
                    <h3>Manufacturers</h3>
                    <?php for($i=0;$i<count($manufacturers);$i++) {?>
                    <input type="checkbox"  onchange="manufactListChange(<?= $manufacturers[$i]['id_manufacturer']?>)" id="manufacturer-<?= $manufacturers[$i]['id_manufacturer']?>" value="<?=$manufacturers[$i]['name_manufacturer']?>">
                    <label><?= $manufacturers[$i]['name_manufacturer']?></label> <br>
                    <?php }?>
                </div>
                <div>
                    <h3>Price</h3>    
                    <label for="price-min">Min:</label>
                    <input type="range" name="price-min" id="price-min" value="0" min="0" max="599" onchange="filterByPrice()">
                    <label for="price-max">Max:</label>
                    <input type="range" name="price-max" id="price-max" value="2000" min="600" max="2000" onchange="filterByPrice()">
                    <p>Price from:</p>
                    <input type="text" id="price-from" readonly>
                    <p>to:</p>
                    <input type="text" id="price-to" readonly>
                    <input type="button" value="Show by price" onclick="showByPrice()">
                </div>
            </section>

            <section id="article-section">
                <section id="article-groups">
                    <div>
                        <div>
                            <img src="../itshop-main/image/MACBOOKAIR.png">
                        </div>
                        <h4>Laptops</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia nisi, suscipit culpa rem laudantium aliquam quia omnis ab! Cumque, totam.</p>
                    </div>

                    <div>
                        <div>
                            <img src="../itshop-main/image/samsung.png">
                        </div>
                        <h4>Mobiles</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia nisi, suscipit culpa rem laudantium aliquam quia omnis ab! Cumque, totam.</p>
                    </div>

                    <div>
                        <div>
                            <img src="../itshop-main/image/razer.png">
                        </div>
                        <h4>Headphones</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia nisi, suscipit culpa rem laudantium aliquam quia omnis ab! Cumque, totam.</p>
                    </div>
                </section>

                <section id="article-cards">
                </section>
                
            </section>
        </section>
    </main>

    <?php 
        include_once 'footer.php';
    ?>
    <script>
        let brands=<?php echo json_encode($brands)?>;
        let products=<?php echo json_encode($products)?>;
        let manufacturers=<?php echo json_encode($manufacturers)?>;
        let types=<?php echo json_encode($types)?>;
    </script>
    <script src="filter.js">
    </script>
